(defproject stigmergy/ikota "0.1.0-SNAPSHOT"
  :description "Small hiccup implementation"
  :url ""
  :license {:name "Apache License 2.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-figwheel "0.5.19"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-ancient "1.0.0-RC3"]
            [lein-cljfmt "0.9.2"]]
  
  :dependencies [[com.taoensso/timbre "6.1.0"]
                 [net.sourceforge.htmlcleaner/htmlcleaner "2.26"]
                 [org.clojure/clojure "1.11.1" :scope "provided"]
                 [org.clojure/clojurescript  "1.11.60" :scope "provided"]]

  :source-paths ["src/cljc" "src/clj"]
  :cljfmt {:paths ["test" "src"]}
  :repl-options {:init-ns user}
  )
