(ns user
  (:require [stigmergy.ikota :as ik]))

(defn a-list [num]
  [:ul
   (for [i (range num)]
     [:li i])])

(defn current-time []
  (let [t (java.util.Date.)]
    [:h2 "The time is " t]))

(defn wassup [name]
  [:div
   [:h1 "Wassup " name]
   [current-time]
   [a-list 3]])

(defn hello []
  [:ul
   (for [i (range 3)]
     [:li i])])

(comment
  (def foo (ik/hiccup->html-str [:div  "wassup" 1 2]))
  (def foo (ik/hiccup->html-str [wassup "world"]))
  (def foo (ik/hiccup->html-str [hello]))
  (def foo (ik/hiccup->html-str [:html {:lang "en"}
                                 [:body nil nil]]))
  (def foo (ik/hiccup->html-str [:html {:lang :en}
                                 [:body
                                  [:h1 1]
                                  [:h1 2]
                                  [:h3 3]]]))

  (def foo (ik/hiccup->html-str [:div [:h1 1] [:h1 2]])))



