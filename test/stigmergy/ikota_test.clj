(ns stigmergy.ikota-test
  (:require [clojure.test :refer :all]
            [stigmergy.ikota :as ik]))

(deftest ikota
  (testing "html-str->hiccup"
    (is (= [:div "1 2"]
           (ik/html-str->hiccup "<div>1 2</div")))

    (is (= [:div
            [:h1 "hi"]]
           (ik/html-str->hiccup "<div><h1>hi</h1></div"))))

  (testing "invariant when applying inverse functions"
    (let [hiccup [:div [:h1 "hello"] [:p "world"]]]
      (is (= hiccup
             (-> hiccup ik/hiccup->html-str ik/html-str->hiccup)))))

  (testing "hiccup->html-str"
    (is (= "<h1>hello world</h1>"
           (ik/hiccup->html-str [:h1 "hello world"])))))
