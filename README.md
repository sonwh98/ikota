# ikota

Ikota is a [Hiccup](https://github.com/weavejester/hiccup) implementation for Clojure/ClojureScript.
The code was extracted from [Mr Clean](https://bitbucket.org/sonwh98/mr-clean/src/master) and
[CHP](https://bitbucket.org/sonwh98/chp/src/master/)

### Why is Ikota better than using regular HTML or other Hiccup implementations?

Because Ikota uses hiccup to represent HTML as clojure data structures giving you the programmer the full
power of Clojure to create, transform and manipulate data structures instead of HTML strings.

Ikota has components which are inspired by [Reagent's](https://github.com/reagent-project/reagent) component model.
Components are simply functions that return hiccup or a closure that returns hiccup. This gives you, the programmer,
the power to compose complex HTML using functional composition.

### Install

Add the following dependency to your `project.clj` file:

    [stigmergy/ikota "0.1.0-SNAPSHOT"]

### Usage

There are only two functions you need to know

* hiccup->html-str
    * converts hiccup into an html string
* hiccup->dom
    * converts hiccup into a DOM object in the browser
    * available only on ClojureScript

### Example

```clojure
(require '[stigmergy.ikota :as ik])

(ik/hiccup->html-str [:h1 "hello world"])
(ik/hiccup->dom [:h1 "wassup world!"]) ;; clojurescript only
```

Component example:
```clojure

(defn a-list [num]
  [:ul
   (for [i (range num)]
     [:li i])])

(defn current-time []
  (let [t (java.util.Date.)]
    [:h2 "The time is " t])
  )

(defn wassup [name]
  [:div
   [:h1 "Wassup " name]
   [current-time]
   [a-list 3]])

(ik/hiccup->html-str [wassup "World"])
```
